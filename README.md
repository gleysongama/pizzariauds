**Procedimento para operar o projeto**

---

## Pré-requsitos para executar e utlizar o projeto

1. Docker
2. Docker compose

## Procedimento para executar e utilizar o projeto após a adição do flyway

1. Na raiz do projeto abra o terminal e digite: docker-compose up (tudo será preparado para utilização).
2. No navegador de sua escolha, acesse: http://localhost:8080/swagger-ui.html
5. Acione o link: pedido-controller para realizar os teste na aplicação.

O projeto permite que sejam criados outros: tamanhos, sabores e opcionais para solicitação de pedidos de novas pizzas
