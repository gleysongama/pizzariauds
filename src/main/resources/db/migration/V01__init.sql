CREATE TABLE `opcional` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `tempo_preparo` double DEFAULT NULL,
  `tipo_opcional` varchar(255) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sabor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `tempo_preparo` double DEFAULT NULL,
  `tipo_sabor` varchar(255) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tamanho` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime(6) DEFAULT NULL,
  `tempo_preparo` double DEFAULT NULL,
  `tipo_tamanho` varchar(255) DEFAULT NULL,
  `updated_at` datetime(6) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;