package br.com.gleysongama.pizzariauds.api.request;

import java.math.BigDecimal;

import br.com.gleysongama.pizzariauds.model.TipoOpcional;

public class OpcionalRequest {
    private TipoOpcional tipoOpcional;

    private BigDecimal valor;

    private Double tempoPreparo;

    public OpcionalRequest() {
        super();
    }

    public OpcionalRequest(TipoOpcional tipoOpcional, BigDecimal valor, Double tempoPreparo) {
        super();
        this.tipoOpcional = tipoOpcional;
        this.valor = valor;
        this.tempoPreparo = tempoPreparo;
    }

    public TipoOpcional getTipoOpcional() {
        return tipoOpcional;
    }

    public void setTipoOpcional(TipoOpcional tipoOpcional) {
        this.tipoOpcional = tipoOpcional;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Double getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(Double tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }
    
}