package br.com.gleysongama.pizzariauds.api;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.pizzariauds.api.request.SaborRequest;
import br.com.gleysongama.pizzariauds.api.response.SaborResponse;
import br.com.gleysongama.pizzariauds.model.Sabor;
import br.com.gleysongama.pizzariauds.service.SaborService;

@RestController
@RequestMapping("/api/v1/sabores")
public class SaborController {
    @Autowired
    private SaborService saborService;

    private ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<SaborResponse>> findAll() {
        List<SaborResponse> response = saborService.findAll()
                                                   .stream()
                                                   .map(sabor -> mapper.map(sabor, SaborResponse.class))
                                                   .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<SaborResponse> create(@RequestBody SaborRequest saborRequest) {
    	SaborResponse saborResponse = mapper.map(saborService.save(mapper.map(saborRequest, Sabor.class)), SaborResponse.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(saborResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SaborResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.map(saborService.findById(id), SaborResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<SaborResponse> update(@PathVariable Long id, @RequestBody SaborRequest saborRequest) {
        Sabor sabor = mapper.map(saborRequest, Sabor.class);
        sabor.setId(id);

        SaborResponse saborResponse = mapper.map(saborService.save(sabor), SaborResponse.class);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(saborResponse);
    }
}