package br.com.gleysongama.pizzariauds.api.request;

import java.util.List;

import br.com.gleysongama.pizzariauds.model.TipoOpcional;
import br.com.gleysongama.pizzariauds.model.TipoSabor;
import br.com.gleysongama.pizzariauds.model.TipoTamanho;

public class PedidoRequest {
    private TipoTamanho tipoTamanho;

    private TipoSabor tipoSabor;

    private List<TipoOpcional> tipoOpcionais;

    public PedidoRequest() {
        super();
    }

    public PedidoRequest(TipoTamanho tipoTamanho, TipoSabor tipoSabor, List<TipoOpcional> tipoOpcionais) {
        super();
        this.tipoTamanho = tipoTamanho;
        this.tipoSabor = tipoSabor;
        this.tipoOpcionais = tipoOpcionais;
    }

    public TipoTamanho getTipoTamanho() {
        return tipoTamanho;
    }

    public void setTipoTamanho(TipoTamanho tipoTamanho) {
        this.tipoTamanho = tipoTamanho;
    }

    public TipoSabor getTipoSabor() {
        return tipoSabor;
    }

    public void setTipoSabor(TipoSabor tipoSabor) {
        this.tipoSabor = tipoSabor;
    }

    public List<TipoOpcional> getTipoOpcionais() {
        return tipoOpcionais;
    }

    public void setTipoOpcionais(List<TipoOpcional> tipoOpcionais) {
        this.tipoOpcionais = tipoOpcionais;
    }

    

}