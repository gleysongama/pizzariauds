package br.com.gleysongama.pizzariauds.api;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.pizzariauds.api.request.TamanhoRequest;
import br.com.gleysongama.pizzariauds.api.response.TamanhoResponse;
import br.com.gleysongama.pizzariauds.model.Tamanho;
import br.com.gleysongama.pizzariauds.service.TamanhoService;

@RestController
@RequestMapping("/api/v1/tamanhos")
public class TamanhoController {
    @Autowired
    private TamanhoService tamanhoService;
    private ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<TamanhoResponse>> findAll() {
        List<TamanhoResponse> response = tamanhoService.findAll()
                                                   .stream()
                                                   .map(tamanho -> mapper.map(tamanho, TamanhoResponse.class))
                                                   .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<TamanhoResponse> create(@RequestBody TamanhoRequest tamanhoRequest) {
    	TamanhoResponse tamanhoResponse = mapper.map(tamanhoService.save(mapper.map(tamanhoRequest, Tamanho.class)), TamanhoResponse.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(tamanhoResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TamanhoResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.map(tamanhoService.findById(id), TamanhoResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<TamanhoResponse> update(@PathVariable Long id, @RequestBody TamanhoRequest tamanhoRequest) {
        Tamanho tamanho = mapper.map(tamanhoRequest, Tamanho.class);
        tamanho.setId(id);

        TamanhoResponse tamanhoResponse = mapper.map(tamanhoService.save(tamanho), TamanhoResponse.class);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(tamanhoResponse);
    }
}