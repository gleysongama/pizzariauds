package br.com.gleysongama.pizzariauds.api.request;

import java.math.BigDecimal;

import br.com.gleysongama.pizzariauds.model.TipoSabor;

public class SaborRequest {
    private TipoSabor tipoSabor;

    private BigDecimal valor;

    private Double tempoPreparo;

    public SaborRequest() {
        super();
    }

    public SaborRequest(TipoSabor tipoSabor, BigDecimal valor, Double tempoPreparo) {
        this.tipoSabor = tipoSabor;
        this.valor = valor;
        this.tempoPreparo = tempoPreparo;
    }

    public TipoSabor getTipoSabor() {
        return tipoSabor;
    }

    public void setTipoSabor(TipoSabor tipoSabor) {
        this.tipoSabor = tipoSabor;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Double getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(Double tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }
    
}