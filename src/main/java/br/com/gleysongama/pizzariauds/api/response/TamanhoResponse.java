package br.com.gleysongama.pizzariauds.api.response;

import java.math.BigDecimal;

import br.com.gleysongama.pizzariauds.model.TipoTamanho;

public class TamanhoResponse {
    private TipoTamanho tipoTamanho;

    private BigDecimal valor;

    private Double tempoPreparo;

    public TamanhoResponse() {
        super();
    }

    public TamanhoResponse(TipoTamanho tipoTamanho, BigDecimal valor, Double tempoPreparo) {
        super();
        this.tipoTamanho = tipoTamanho;
        this.valor = valor;
        this.tempoPreparo = tempoPreparo;
    }

    public TipoTamanho getTipoTamanho() {
        return tipoTamanho;
    }

    public void setTipoTamanho(TipoTamanho tipoTamanho) {
        this.tipoTamanho = tipoTamanho;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Double getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(Double tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }
    
}