package br.com.gleysongama.pizzariauds.api.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PedidoResponse {
    private String tamanho;

    private String sabor;

    private List<String> personalizacoes = new ArrayList<String>();

    private BigDecimal valorTotal;

    private Double tempoPreparo;

    public PedidoResponse() {
        super();
    }

    public PedidoResponse(String tamanho, String sabor, List<String> personalizacoes,
            BigDecimal valorTotal, Double tempoPreparo) {
        this.tamanho = tamanho;
        this.sabor = sabor;
        this.personalizacoes = personalizacoes;
        this.valorTotal = valorTotal;
        this.tempoPreparo = tempoPreparo;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public List<String> getPersonalizacoes() {
        return personalizacoes;
    }

    public void setPersonalizacoes(List<String> personalizacoes) {
        this.personalizacoes = personalizacoes;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Double getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(Double tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }
          
}