package br.com.gleysongama.pizzariauds.api;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.pizzariauds.api.request.PedidoRequest;
import br.com.gleysongama.pizzariauds.api.response.PedidoResponse;
import br.com.gleysongama.pizzariauds.service.PedidoService;

@RestController
@RequestMapping("/api/v1/pedidos")
public class PedidoController {
    @Autowired
    private PedidoService pedidoService;

    private ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<PedidoResponse>> findAll() {
        List<PedidoResponse> response = pedidoService.findAll()
                                                         .stream()
                                                         .map(pedido -> mapper.map(pedido, PedidoResponse.class))
                                                         .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<PedidoResponse> create(@RequestBody PedidoRequest pedidoRequest) {
    	PedidoResponse pedidoResponse = mapper.map(pedidoService.save(pedidoRequest), PedidoResponse.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(pedidoResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PedidoResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.map(pedidoService.findById(id), PedidoResponse.class));
    }

}