package br.com.gleysongama.pizzariauds.api;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gleysongama.pizzariauds.api.request.OpcionalRequest;
import br.com.gleysongama.pizzariauds.api.response.OpcionalResponse;
import br.com.gleysongama.pizzariauds.model.Opcional;
import br.com.gleysongama.pizzariauds.service.OpcionalService;

@RestController
@RequestMapping("/api/v1/opcionais")
public class OpcionalController {
    @Autowired
    private OpcionalService opcionalService;

    private ModelMapper mapper = new ModelMapper();

    @GetMapping
    public ResponseEntity<List<OpcionalResponse>> findAll() {
        List<OpcionalResponse> response = opcionalService.findAll()
                                                         .stream()
                                                         .map(opcional -> mapper.map(opcional, OpcionalResponse.class))
                                                         .collect(Collectors.toList());
        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<OpcionalResponse> create(@RequestBody OpcionalRequest opcionalRequest) {
    	OpcionalResponse opcionalResponse = mapper.map(opcionalService.save(mapper.map(opcionalRequest, Opcional.class)), OpcionalResponse.class);

        return ResponseEntity.status(HttpStatus.CREATED).body(opcionalResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OpcionalResponse> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.map(opcionalService.findById(id), OpcionalResponse.class));
    }

    @PutMapping("/{id}")
    public ResponseEntity<OpcionalResponse> update(@PathVariable Long id, @RequestBody OpcionalRequest opcionalRequest) {
        Opcional opcional = mapper.map(opcionalRequest, Opcional.class);
        opcional.setId(id);

        OpcionalResponse opcionalResponse = mapper.map(opcionalService.save(opcional), OpcionalResponse.class);

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(opcionalResponse);
    }

}