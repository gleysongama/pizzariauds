package br.com.gleysongama.pizzariauds.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.pizzariauds.model.Tamanho;
import br.com.gleysongama.pizzariauds.repository.TamanhoRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TamanhoService {
    @Autowired
    private TamanhoRepository tamanhoRepository;

    public List<Tamanho> findAll() {
        return tamanhoRepository.findAll();
    }

    public Tamanho findById(Long id) {
        return tamanhoRepository.findById(id).get();
    }

    public Tamanho save(Tamanho Tamanho) {
        return tamanhoRepository.save(Tamanho);
    }

    public void deleteById(Long id) {
        tamanhoRepository.deleteById(id);
    }
}