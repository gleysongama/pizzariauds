package br.com.gleysongama.pizzariauds.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.pizzariauds.api.request.PedidoRequest;
import br.com.gleysongama.pizzariauds.model.Pedido;
import br.com.gleysongama.pizzariauds.model.Pizza;
import br.com.gleysongama.pizzariauds.repository.OpcionalRepository;
import br.com.gleysongama.pizzariauds.repository.PedidoRepository;
import br.com.gleysongama.pizzariauds.repository.SaborRepository;
import br.com.gleysongama.pizzariauds.repository.TamanhoRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PedidoService {
    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    TamanhoRepository tamanhoRepository;

    @Autowired
    SaborRepository saborRepository;

    @Autowired
    OpcionalRepository opcionalRepository;

    public List<Pedido> findAll() {
        return pedidoRepository.findAll();
    }

    public Pedido findById(Long id) {
        return pedidoRepository.findById(id).get();
    }

    public Pedido save(PedidoRequest pedidoRequest) {
        Pizza pizza  = prepararPizza(pedidoRequest);
        Pedido pedido = new Pedido();

        pedido.setTamanho(pizza.getTamanho().getTipoTamanho().getTipo());
        pedido.setSabor(pizza.getSabor().getTipoSabor().getTipo());
        
        pedido.setPersonalizacoes( 
            pizza.getOpcionais()
                 .stream()
                 .map(opcional -> opcional.getTipoOpcional().getTipo())
                 .collect(Collectors.toList())
        );

        pedido.setValorTotal(calcularValorTotal(pizza));
        pedido.setTempoPreparo(calcularTempoPreparo(pizza));
        pedido.setPizza(pizza);

        return pedidoRepository.save(pedido);
    }

    public void deleteById(Long id) {
        pedidoRepository.deleteById(id);
    }

    /****************** MONTAGEM DE PIZZA E TOTALIZADORES ******************/

    private Pizza prepararPizza(PedidoRequest pedido) {
        Pizza pizza = new Pizza();
        try {
            pizza.setTamanho(tamanhoRepository.findByTipoTamanho(pedido.getTipoTamanho()));
            pizza.setSabor(saborRepository.findByTipoSabor(pedido.getTipoSabor()));
            pizza.setOpcionais(
                pedido.getTipoOpcionais()
                      .stream()
                      .map(tipoOpicional -> opcionalRepository.findByTipoOpcional(tipoOpicional))
                      .collect(Collectors.toList())
            );
        } catch(Exception ex) {
            pizza = null;
        }

        return pizza;
    }

    private BigDecimal calcularValorTotal(Pizza pizza){
        BigDecimal valorTotal = new BigDecimal(BigDecimal.ZERO.toString());

        valorTotal = valorTotal.add(pizza.getTamanho().getValor());
        valorTotal = valorTotal.add(pizza.getSabor().getValor());
        valorTotal = valorTotal.add(pizza.getOpcionais()
                               .stream()
                               .map(opcional -> opcional.getValor())
                               .reduce(BigDecimal.ZERO, BigDecimal::add));
        
        return valorTotal;
    }

    private Double calcularTempoPreparo(Pizza pizza) {
        Double tempoPreparo = 0.0;

        tempoPreparo = Double.sum(pizza.getTamanho().getTempoPreparo(), pizza.getSabor().getTempoPreparo());
        tempoPreparo = Double.sum(tempoPreparo, pizza.getOpcionais()
                                                     .stream()
                                                     .map(opcional -> opcional.getTempoPreparo())
                                                     .reduce(0.0, Double::sum));

        return tempoPreparo;
    }
}