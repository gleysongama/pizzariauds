package br.com.gleysongama.pizzariauds.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.pizzariauds.model.Opcional;
import br.com.gleysongama.pizzariauds.repository.OpcionalRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class OpcionalService {
    @Autowired
    private OpcionalRepository opcionalRepository;

    public List<Opcional> findAll() {
        return opcionalRepository.findAll();
    }

    public Opcional findById(Long id) {
        return opcionalRepository.findById(id).get();
    }

    public Opcional save(Opcional opcional) {
        return opcionalRepository.save(opcional);
    }

    public void deleteById(Long id) {
        opcionalRepository.deleteById(id);
    }
}