package br.com.gleysongama.pizzariauds.model;

public enum TipoOpcional {
    EXTRA_BACON("Extra bacon"),
    SEM_CEBOLA("Sem cebola"),
    BORDA_RECHEADA("Borda recheada");

    private String tipo;

    TipoOpcional(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return this.tipo;
    }
}