package br.com.gleysongama.pizzariauds.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Opcional {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TipoOpcional tipoOpcional;

    private BigDecimal valor;

    private Double tempoPreparo;

    @CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;
	
    public Opcional() {
		super();
	}

	public Opcional(TipoOpcional tipoOpcional, BigDecimal valor, Double tempoPreparo) {
        this.tipoOpcional = tipoOpcional;
        this.valor = valor;
        this.tempoPreparo = tempoPreparo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoOpcional getTipoOpcional() {
        return tipoOpcional;
    }

    public void setOpcional(TipoOpcional tipoOpcional) {
        this.tipoOpcional = tipoOpcional;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Double getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(Double tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tipoOpcional == null) ? 0 : tipoOpcional.hashCode());
        result = prime * result + ((tempoPreparo == null) ? 0 : tempoPreparo.hashCode());
        result = prime * result + ((valor == null) ? 0 : valor.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Opcional other = (Opcional) obj;
        if (tipoOpcional != other.tipoOpcional)
            return false;
        if (tempoPreparo == null) {
            if (other.tempoPreparo != null)
                return false;
        } else if (!tempoPreparo.equals(other.tempoPreparo))
            return false;
        if (valor == null) {
            if (other.valor != null)
                return false;
        } else if (!valor.equals(other.valor))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Opcional [createdAt=" + createdAt + ", id=" + id + ", tipoOpcional=" + tipoOpcional + ", tempoPreparo="
                + tempoPreparo + ", updatedAt=" + updatedAt + ", valor=" + valor + "]";
    }
    
}