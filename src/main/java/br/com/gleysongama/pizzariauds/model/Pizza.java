package br.com.gleysongama.pizzariauds.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
public class Pizza {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tamanho_id")
    private Tamanho tamanho;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sabor_id")
    private Sabor sabor;

    @OneToMany(orphanRemoval = true)
    private List<Opcional> opcionais;

    @OneToOne(mappedBy = "pizza")
    private Pedido pedido;

    @CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;

    public Pizza() {
        super();
    }
}