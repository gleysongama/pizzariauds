package br.com.gleysongama.pizzariauds.model;

public enum TipoTamanho {
    PEQUENA("Pequena"),
    MEDIA("Media"),
    GRANDE("Grande");

    private final String tipo;

    TipoTamanho(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return this.tipo;
    }
}