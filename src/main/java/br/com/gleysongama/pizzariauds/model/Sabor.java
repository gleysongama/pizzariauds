package br.com.gleysongama.pizzariauds.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@Entity
public class Sabor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private TipoSabor tipoSabor;

    private BigDecimal valor;

    private Double tempoPreparo;

    @CreationTimestamp
	private Date createdAt;

	@UpdateTimestamp
    private Date updatedAt;
	
    public Sabor() {
		super();
	}

	public Sabor(TipoSabor tipoSabor, BigDecimal valor, Double tempoPreparo) {
        super();
        this.tipoSabor = tipoSabor;
        this.valor = valor;
        this.tempoPreparo = tempoPreparo;
    }

    
}