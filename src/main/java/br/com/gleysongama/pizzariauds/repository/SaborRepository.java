package br.com.gleysongama.pizzariauds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.pizzariauds.model.Sabor;
import br.com.gleysongama.pizzariauds.model.TipoSabor;

public interface SaborRepository extends JpaRepository<Sabor, Long> {
    public Sabor findByTipoSabor(TipoSabor tipoSabor);
}