package br.com.gleysongama.pizzariauds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.pizzariauds.model.Tamanho;
import br.com.gleysongama.pizzariauds.model.TipoTamanho;

public interface TamanhoRepository extends JpaRepository<Tamanho, Long> {
    public Tamanho findByTipoTamanho(TipoTamanho tipoTamanho);
}