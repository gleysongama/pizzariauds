package br.com.gleysongama.pizzariauds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.pizzariauds.model.Opcional;
import br.com.gleysongama.pizzariauds.model.TipoOpcional;

public interface OpcionalRepository extends JpaRepository<Opcional, Long> {
    public Opcional findByTipoOpcional(TipoOpcional tipoOpcional);
}