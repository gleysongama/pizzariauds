package br.com.gleysongama.pizzariauds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.pizzariauds.model.Pizza;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {
    
}