package br.com.gleysongama.pizzariauds.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.pizzariauds.model.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {
    
}